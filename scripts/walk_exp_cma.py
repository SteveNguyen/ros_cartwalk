#!/usr/bin/python
# -*- coding: utf-8 -*-

#
#  File Name	: 'walk_exp.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@labri.fr
#  Created	: mercredi, mars  4 2015
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
#


import sys
import roslib
roslib.load_manifest('ros_cartwalk')
import rospy

from gazebo_msgs.msg import LinkStates
from geometry_msgs.msg import Pose
from std_srvs.srv import Empty
from ros_cartwalk.srv import SetPhase
import numpy as np
import time
import tf
import cma

import dynamic_reconfigure.client

poses = dict()
twists = dict()


def gz_cb(msg):

    for i in range(len(msg.name)):
        # print msg.name[i]
        poses[msg.name[i]] = msg.pose[i]
        twists[msg.name[i]] = msg.twist[i]


def config_cb(config):
    # print config
    pass


class walk():

    def __init__(self, params, params_list, params_range, poses, twists, reset_s, setphase_s, params_client, flog='cma_walk.dat'):
        self.params = params
        self.params_list = params_list
        self.params_range = params_range
        self.poses = poses
        self.twists = twists
        self.reset_s = reset_s
        self.setphase_s = setphase_s
        self.params_client = params_client
        self.flog = open(flog, 'a+')
        # self.p0range = [1.0, 3.5]
        # self.p1range = [-5.0, 25.0]
        # self.p2range = [-5.0, 16.0]

        # s = '#timeGain hipOffset head_roll_std head_pitch_std head_yaw_std
        # distance max_t fall\n'

        s = '#'
        for it in range(len(self.params_list)):
            s += '%s ' % (self.params_list[it])
        s += 'head_roll_std head_pitch_std head_yaw_std distance max_t score fall\n'

        self.flog.write(s)

    def eval(self, newparams):
        # param[0]=timegain
        # param[1]=hipoffset
        # param[2]=step

        penalty = []

        it = 0
        for it in range(len(newparams)):
            if newparams[it] < self.params_range[it][0]:
                penalty.append((newparams[it] - self.params_range[it][0]) ** 2)
                newparams[it] = self.params_range[it][0]
                print 'penalty on param %d (inf)' % (it)
            elif newparams[it] > self.params_range[it][1]:
                penalty.append((newparams[it] - self.params_range[it][1]) ** 2)
                newparams[it] = self.params_range[it][1]
                print 'penalty on param %d (sup)' % (it)

        # simu
        self.params['dynamic_enabled'] = False
        print 'STOP'
        fall = 0

        self.params_client.update_configuration(self.params)

        # 1 reset simu
        self.setphase_s(0.0)
        self.reset_s()
        time.sleep(2)
        rospy.sleep(1)
        # 2 change params

        self.params['dynamic_enabled'] = True
        s = '\t'
        for it in range(len(self.params_list)):
            # print it, self.params_list[it], newparams[it]
            self.params[self.params_list[it]] = newparams[it]
            s += '%s: %f ' % (self.params_list[it], newparams[it])

        s += '\n'

        # 3 start simu

        # workaround for the step
        goalstep = params['dynamic_step']
        step_ramp = np.linspace(0.0, goalstep, 5)  # 5s
        params['dynamic_step'] = 0.0

        self.params_client.update_configuration(params)
                # 4 record head
        print 'START:'
        print s

        t = 0

        head_r = []
        head_p = []
        head_y = []
        head_z = self.poses['SigmaBan::tilt_link'].position.z
        dist = 0.0

        it = 0
        itstep = 0
        while t < 15.0 and head_z > 0.2:

            if (it % 100) == 0.0 and it < 500:

                params['dynamic_step'] = step_ramp[itstep]
                itstep += 1
                self.params_client.update_configuration(params)

            head_z = poses['SigmaBan::tilt_link'].position.z

            quat = self.poses['SigmaBan::tilt_link'].orientation
            angles = tf.transformations.euler_from_quaternion(
                [quat.x, quat.y, quat.z, quat.w])
            head_r.append(angles[0])
            head_p.append(angles[1])
            head_y.append(angles[2])

            # dist += np.sqrt(twists['SigmaBan::trunk_link'].linear.x ** 2 + twists[
                # 'SigmaBan::trunk_link'].linear.y ** 2) * 0.01

                    # print angles

                # while(not rospy.is_shutdown()):
                #     print poses['SigmaBan::tilt_link']

                # 5 if t>20s or head_z<0.3 stop
                # 6 mean and std head: score=std
                # 7 goto 1
            rospy.sleep(0.01)
            t += 0.01
            it += 1

        head_r = np.array(head_r)
        head_p = np.array(head_p)
        head_y = np.array(head_y)

        if head_z < 0.2:
            print "FALL"
            fall = 1

        dist = np.sqrt(poses['SigmaBan::tilt_link'].position.x ** 2 + poses[
            'SigmaBan::tilt_link'].position.y ** 2)

                # print head_r.std()

        s = ''
        for it in range(len(self.params_list)):
            s += '%f ' % (newparams[it])

        score = (1.0 / dist) + np.array(penalty).sum() + \
            head_r.std() + head_p.std() + head_y.std() + \
            self.params['static_timeGain'] * 0.01

        if fall:
            score += 10.0

        s += '%f %f %f %f %f %f %d\n' % (
            head_r.std(), head_p.std(), head_y.std(), dist, t, score, fall)

        self.flog.write(s)

        print 'DISTANCE: %f, score: %f' % (dist, score)
        return score


class params_setter():

    def __init__(self, pid=False):
        # self.params_list = params_list
        self.pid = pid
        self.pid_client = {}
        self.params_client = dynamic_reconfigure.client.Client(
            '/SigmaBan/test_ros_cartwalk_bridge', timeout=30, config_callback=config_cb)
        self.pid_params = {}

        if self.pid:
            self.pid_params = rospy.get_param(
                '/SigmaBan/rleg_k_p_controller/pid')
            self.p = self.pid_params['p']
            pid_list = ['/SigmaBan/rleg_k_p_controller/pid',
                        '/SigmaBan/rleg_h_y_controller/pid',
                        '/SigmaBan/rleg_h_r_controller/pid',
                        '/SigmaBan/rleg_h_p_controller/pid',
                        '/SigmaBan/rleg_a_r_controller/pid',
                        '/SigmaBan/rleg_a_p_controller/pid',
                        '/SigmaBan/rarm_s_r_controller/pid',
                        '/SigmaBan/rarm_s_p_controller/pid',
                        '/SigmaBan/rarm_e_p_controller/pid',
                        '/SigmaBan/lleg_k_p_controller/pid',
                        '/SigmaBan/lleg_h_y_controller/pid',
                        '/SigmaBan/lleg_h_r_controller/pid',
                        '/SigmaBan/lleg_h_p_controller/pid',
                        '/SigmaBan/lleg_a_r_controller/pid',
                        '/SigmaBan/lleg_a_p_controller/pid',
                        '/SigmaBan/larm_s_r_controller/pid',
                        '/SigmaBan/larm_s_p_controller/pid',
                        '/SigmaBan/larm_e_p_controller/pid',
                        '/SigmaBan/head_y_controller/pid',
                        '/SigmaBan/head_p_controller/pid']

            for p in pid_list:

                self.pid_client[p] = dynamic_reconfigure.client.Client(
                    p, timeout=30, config_callback=config_cb)

    def update_configuration(self, params):

        if 'pid' in params:
            self.p = params['pid']
            params.pop('pid', None)

        self.params_client.update_configuration(params)
        if self.pid:
            self.pid_params['p'] = self.p
            for k, c in self.pid_client.iteritems():
                c.update_configuration(self.pid_params)


if __name__ == '__main__':

    rospy.init_node('walk_exp')

    rospy.Subscriber('/gazebo/link_states', LinkStates, gz_cb)
    reset_s = rospy.ServiceProxy('/gazebo/reset_simulation', Empty)
    setphase_s = rospy.ServiceProxy('/SigmaBan/set_phase', SetPhase)

    params = rospy.get_param('SigmaBan/test_ros_cartwalk_bridge')

    # params_client = dynamic_reconfigure.client.Client(
    #     '/SigmaBan/test_ros_cartwalk_bridge', timeout=30, config_callback=config_cb)

    params_list = ['static_timeGain',
                   'static_hipOffset', 'dynamic_step', 'static_swingPhase', 'static_swingGain', 'pid']

    params_client = params_setter(pid=True)

    params['pid'] = params_client.p

    params_range = [[1.0, 3.5], [-5.0, 25.0],
                   [-5.0, 16.0], [-0.5, 1.5],
                   [0, 4], [0, 100]]

    exp = walk(params, params_list, params_range,
               poses, twists, reset_s, setphase_s, params_client)

    initial_guess = [3.0, 12.0, 3.0, 0.0, 1.0, 20.0]
    variance = 0.5
    res = cma.fmin(exp.eval, initial_guess, variance)

    print 'RESULT'
    print res
    cma.plot()
    cma.savefig('cma.png')
    cma.closefig()
