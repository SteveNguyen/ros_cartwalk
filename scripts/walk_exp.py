#!/usr/bin/python
# -*- coding: utf-8 -*-

#
#  File Name	: 'walk_exp.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@labri.fr
#  Created	: mercredi, mars  4 2015
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
#


import sys
import roslib
roslib.load_manifest('ros_cartwalk')
import rospy

from gazebo_msgs.msg import LinkStates
from geometry_msgs.msg import Pose
from std_srvs.srv import Empty
import numpy as np
import time
import tf

import dynamic_reconfigure.client

poses = dict()
twists = dict()


def gz_cb(msg):

    for i in range(len(msg.name)):
        # print msg.name[i]
        poses[msg.name[i]] = msg.pose[i]
        twists[msg.name[i]] = msg.twist[i]


def config_cb(config):
    # print config
    pass

if __name__ == '__main__':

    rospy.init_node('walk_exp')

    rospy.Subscriber('/gazebo/link_states', LinkStates, gz_cb)
    reset_s = rospy.ServiceProxy('/gazebo/reset_simulation', Empty)

    params = rospy.get_param('SigmaBan/test_ros_cartwalk_bridge')
    params_client = dynamic_reconfigure.client.Client(
        '/SigmaBan/test_ros_cartwalk_bridge', timeout=30, config_callback=config_cb)

    timegain = params['static_timeGain']
    hipoff = params['static_hipOffset']

    fd = open('parameters.dat', 'a+')
    s = '#timeGain hipOffset head_roll_std head_pitch_std head_yaw_std distance max_t fall\n'
    fd.write(s)

    # for hipoff in np.linspace(- 5.0, 25.0, 20):
    for hipoff in np.linspace(12.0, 22.0, 10):

        # for timegain in np.linspace(1.0, 3.5, 20):
        for timegain in np.linspace(2.5, 3.5, 10):

            for nb in range(10):
                # 0 reset walk

                # rospy.set_param(
                #     '/SigmaBan/test_ros_cartwalk_bridge/dynamic_enabled', False)

                params['dynamic_enabled'] = False
                print 'STOP'
                fall = 0
                # params_s(params)
                params_client.update_configuration(params)
                # print params
                # 1 reset simu
                reset_s()
                time.sleep(2)
                rospy.sleep(1)
                # 2 change params
                # timegain += 0.1
                # rospy.set_param(
                #     '/SigmaBan/test_ros_cartwalk_bridge/static_timeGain', timegain)

                # 3 start walk
                # rospy.set_param(
                #     '/SigmaBan/test_ros_cartwalk_bridge/dynamic_enabled', True)

                params['dynamic_enabled'] = True
                params['static_timeGain'] = timegain
                params['static_hipOffset'] = hipoff
                params['dynamic_step'] = 3.0

                params_client.update_configuration(params)
                # 4 record head
                print 'START, timegain: %f, hipoff: %f' % (timegain, hipoff)
                t = 0

                # quat = poses['SigmaBan::tilt_link'].orientation
                # angles = tf.transformations.euler_from_quaternion(
                #     [quat.x, quat.y, quat.z, quat.w])

                # print angles

                head_r = []
                head_p = []
                head_y = []
                head_z = poses['SigmaBan::tilt_link'].position.z
                dist = 0.0

                while t < 10.0 and head_z > 0.2:
                    head_z = poses['SigmaBan::tilt_link'].position.z
                # for i in range(50):
                    quat = poses['SigmaBan::tilt_link'].orientation
                    angles = tf.transformations.euler_from_quaternion(
                        [quat.x, quat.y, quat.z, quat.w])
                    head_r.append(angles[0])
                    head_p.append(angles[1])
                    head_y.append(angles[2])

                    dist += np.sqrt(twists['SigmaBan::trunk_link'].linear.x ** 2 + twists[
                                    'SigmaBan::trunk_link'].linear.y ** 2) * 0.01

                    # print angles

                # while(not rospy.is_shutdown()):
                #     print poses['SigmaBan::tilt_link']

                # 5 if t>20s or head_z<0.3 stop
                # 6 mean and std head: score=std
                # 7 goto 1
                    rospy.sleep(0.01)
                    t += 0.01
                head_r = np.array(head_r)
                head_p = np.array(head_p)
                head_y = np.array(head_y)

                if head_z < 0.2:
                    print "FALL"
                    fall = 1

                # dist = np.sqrt(poses['SigmaBan::tilt_link'].position.x ** 2 + poses[
                #     'SigmaBan::tilt_link'].position.y ** 2)
                # print head_r.std()
                s = '%f %f %f %f %f %f %f %d\n' % (
                    timegain, hipoff, head_r.std(), head_p.std(), head_y.std(), dist, t,  fall)
                fd.write(s)
    fd.close()
