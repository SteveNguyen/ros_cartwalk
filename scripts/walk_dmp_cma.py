#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import roslib
roslib.load_manifest('ros_cartwalk')
import rospy

from gazebo_msgs.msg import LinkStates
from geometry_msgs.msg import Pose
from std_srvs.srv import Empty
from ros_cartwalk.srv import SetPhase
import numpy as np
import time
import tf
import cma

import dynamic_reconfigure.client

from std_msgs.msg import Float64

import pydmps.dmp_rhythmic as DMP

robot_namespace = '/SigmaBan/'

robot_topics = [robot_namespace + 'lleg_h_r_controller/command',
                robot_namespace + 'lleg_h_p_controller/command',
                robot_namespace + 'lleg_h_y_controller/command',
                robot_namespace + 'lleg_k_p_controller/command',
                robot_namespace + 'lleg_a_r_controller/command',
                robot_namespace + 'lleg_a_p_controller/command',
                robot_namespace + 'rleg_h_r_controller/command',
                robot_namespace + 'rleg_h_p_controller/command',
                robot_namespace + 'rleg_h_y_controller/command',
                robot_namespace + 'rleg_k_p_controller/command',
                robot_namespace + 'rleg_a_r_controller/command',
                robot_namespace + 'rleg_a_p_controller/command']


def read_data(filename):

    data = []
    f = open(filename, 'r')

    for l in f:
        if l[0] != '#':
            l = l.replace('\n', '')
            l = l.split(' ')
            line = []
            for ll in l:
                if ll != '':
                    line.append(float(ll))
            data.append(np.array(line))
    return np.array(data)


poses = dict()
twists = dict()


def gz_cb(msg):

    for i in range(len(msg.name)):
        # print msg.name[i]
        poses[msg.name[i]] = msg.pose[i]
        twists[msg.name[i]] = msg.twist[i]


class walk():

    def __init__(self, dmps, publishers, poses, twists, reset_s, flog='cma_walk.dat'):

        self.dmps = dmps
        self.publishers = publishers
        self.poses = poses
        self.twists = twists
        self.reset_s = reset_s
        # self.setphase_s = setphase_s
        # self.params_client = params_client
        self.flog = open(flog, 'a+')
        # self.p0range = [1.0, 3.5]
        # self.p1range = [-5.0, 25.0]
        # self.p2range = [-5.0, 16.0]
        self.tau = 1.0
        # s = '#timeGain hipOffset head_roll_std head_pitch_std head_yaw_std
        # distance max_t fall\n'

        s = '#'
        # for it in range(len(self.params_list)):
        #     s += '%s ' % (self.params_list[it])
        s += 'head_roll_std head_pitch_std head_yaw_std distance max_t score fall\n'

        self.flog.write(s)

    def eval(self, newparams):
        # param[0]=timegain
        # param[1]=hipoffset
        # param[2]=step

        self.tau = newparams[- 1]
        self.dmps.w = newparams[:- 1].reshape(self.dmps.w.shape)
        penalty = []

        # it = 0
        # for it in range(len(newparams)):
        #     if newparams[it] < self.params_range[it][0]:
        #         penalty.append((newparams[it] - self.params_range[it][0]) ** 2)
        #         newparams[it] = self.params_range[it][0]
        #         print 'penalty on param %d (inf)' % (it)
        #     elif newparams[it] > self.params_range[it][1]:
        #         penalty.append((newparams[it] - self.params_range[it][1]) ** 2)
        #         newparams[it] = self.params_range[it][1]
        #         print 'penalty on param %d (sup)' % (it)

        # simu
        # self.params['dynamic_enabled'] = False
        # print 'STOP'
        fall = 0

        # self.params_client.update_configuration(self.params)

        # 1 reset simu
        self.dmps.reset_state()
        i = 0
        step = self.dmps.step(self.tau)[0]
        for i in range(len(robot_topics)):
            self.publishers[robot_topics[i]].publish(step[i])
            i += 1
        time.sleep(1)
        self.reset_s()
        time.sleep(1)
        # rospy.sleep(1)

        # 2 change params

        # 3 start simu

        # 4 record head
        print 'START:'
        # print s

        t = 0

        head_r = []
        head_p = []
        head_y = []
        head_z = self.poses['SigmaBan::tilt_link'].position.z
        dist = 0.0

        it = 0
        itstep = 0
        # r = rospy.Rate(50)

        while t < 15.0 and head_z > 0.2:

            head_z = poses['SigmaBan::tilt_link'].position.z

            quat = self.poses['SigmaBan::tilt_link'].orientation
            angles = tf.transformations.euler_from_quaternion(
                [quat.x, quat.y, quat.z, quat.w])
            head_r.append(angles[0])
            head_p.append(angles[1])
            head_y.append(angles[2])

            i = 0
            step = self.dmps.step(self.tau)[0]
            for i in range(len(robot_topics)):
                self.publishers[robot_topics[i]].publish(step[i])
                i += 1

            # dist += np.sqrt(twists['SigmaBan::trunk_link'].linear.x ** 2 + twists[
                # 'SigmaBan::trunk_link'].linear.y ** 2) * 0.01

                    # print angles

                # while(not rospy.is_shutdown()):
                #     print poses['SigmaBan::tilt_link']

                # 5 if t>20s or head_z<0.3 stop
                # 6 mean and std head: score=std
                # 7 goto 1

            # r.sleep()
            rospy.sleep(1.0 / 50.0)
            t += 1.0 / 50.0
            it += 1

        head_r = np.array(head_r)
        head_p = np.array(head_p)
        head_y = np.array(head_y)

        if head_z < 0.2:
            print "FALL"
            fall = 1

        dist = np.sqrt(poses['SigmaBan::tilt_link'].position.x ** 2 + poses[
            'SigmaBan::tilt_link'].position.y ** 2)

                # print head_r.std()

        s = ''
        # for it in range(len(self.params_list)):
        #     s += '%f ' % (newparams[it])

        score = (1.0 / dist) + head_r.std() + head_p.std() + head_y.std()

        if fall:
            score += 10.0

        s += '%f %f %f %f %f %f %d\n' % (
            head_r.std(), head_p.std(), head_y.std(), dist, t, score, fall)

        self.flog.write(s)

        print 'DISTANCE: %f, score: %f' % (dist, score)
        return score


class params_setter():

    def __init__(self, pid=False):
        # self.params_list = params_list
        self.pid = pid
        self.pid_client = {}
        self.params_client = dynamic_reconfigure.client.Client(
            '/SigmaBan/test_ros_cartwalk_bridge', timeout=30, config_callback=config_cb)
        self.pid_params = {}

        if self.pid:
            self.pid_params = rospy.get_param(
                '/SigmaBan/rleg_k_p_controller/pid')
            self.p = self.pid_params['p']
            pid_list = ['/SigmaBan/rleg_k_p_controller/pid',
                        '/SigmaBan/rleg_h_y_controller/pid',
                        '/SigmaBan/rleg_h_r_controller/pid',
                        '/SigmaBan/rleg_h_p_controller/pid',
                        '/SigmaBan/rleg_a_r_controller/pid',
                        '/SigmaBan/rleg_a_p_controller/pid',
                        '/SigmaBan/rarm_s_r_controller/pid',
                        '/SigmaBan/rarm_s_p_controller/pid',
                        '/SigmaBan/rarm_e_p_controller/pid',
                        '/SigmaBan/lleg_k_p_controller/pid',
                        '/SigmaBan/lleg_h_y_controller/pid',
                        '/SigmaBan/lleg_h_r_controller/pid',
                        '/SigmaBan/lleg_h_p_controller/pid',
                        '/SigmaBan/lleg_a_r_controller/pid',
                        '/SigmaBan/lleg_a_p_controller/pid',
                        '/SigmaBan/larm_s_r_controller/pid',
                        '/SigmaBan/larm_s_p_controller/pid',
                        '/SigmaBan/larm_e_p_controller/pid',
                        '/SigmaBan/head_y_controller/pid',
                        '/SigmaBan/head_p_controller/pid']

            for p in pid_list:

                self.pid_client[p] = dynamic_reconfigure.client.Client(
                    p, timeout=30, config_callback=config_cb)

    def update_configuration(self, params):

        if 'pid' in params:
            self.p = params['pid']
            params.pop('pid', None)

        self.params_client.update_configuration(params)
        if self.pid:
            self.pid_params['p'] = self.p
            for k, c in self.pid_client.iteritems():
                c.update_configuration(self.pid_params)


if __name__ == '__main__':

    rospy.init_node('walk_exp_dmp')

    rospy.Subscriber('/gazebo/link_states', LinkStates, gz_cb)
    reset_s = rospy.ServiceProxy('/gazebo/reset_simulation', Empty)

    data = read_data(sys.argv[1])
    data = data[: 188].transpose()  # 9 periods
    # print data.shape

    dmps = DMP.DMPs_rhythmic(
        dmps=12, bfs=100, dt=1.0 / 50.0 * 2.0 * np.pi * 2.4 / 9)
    # print dmps.timesteps
    dmps.imitate_path(y_des=data)

    publishers = {}
    for c in range(data.shape[0]):

        publishers[robot_topics[c]] = rospy.Publisher(robot_topics[c], Float64)

    exp = walk(dmps, publishers, poses, twists, reset_s)

    # print dmps.w.shape
    initial_guess = dmps.w.flatten()
    initial_guess = np.append(initial_guess, [1.0])

    variance = 0.1

    res = cma.fmin(exp.eval, initial_guess, variance)

    np.save('dmpw.npy', dmps.w)
    print 'TAU: ', exp.tau
    print 'RESULT'
    print res
    cma.plot()
    cma.savefig('cma.png')
    cma.closefig()
