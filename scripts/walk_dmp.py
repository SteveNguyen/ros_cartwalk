#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import roslib
roslib.load_manifest('ros_cartwalk')
import rospy

from std_msgs.msg import Float64

import pydmps.dmp_rhythmic as DMP
import numpy as np
import sys

robot_namespace = '/SigmaBan/'

robot_topics = [robot_namespace + 'lleg_h_r_controller/command',
                robot_namespace + 'lleg_h_p_controller/command',
                robot_namespace + 'lleg_h_y_controller/command',
                robot_namespace + 'lleg_k_p_controller/command',
                robot_namespace + 'lleg_a_r_controller/command',
                robot_namespace + 'lleg_a_p_controller/command',
                robot_namespace + 'rleg_h_r_controller/command',
                robot_namespace + 'rleg_h_p_controller/command',
                robot_namespace + 'rleg_h_y_controller/command',
                robot_namespace + 'rleg_k_p_controller/command',
                robot_namespace + 'rleg_a_r_controller/command',
                robot_namespace + 'rleg_a_p_controller/command']


def read_data(filename):

    data = []
    f = open(filename, 'r')

    for l in f:
        if l[0] != '#':
            l = l.replace('\n', '')
            l = l.split(' ')
            line = []
            for ll in l:
                if ll != '':
                    line.append(float(ll))
            data.append(np.array(line))
    return np.array(data)


if __name__ == '__main__':

    import matplotlib.pyplot as plt
    rospy.init_node('walk_dmp')

    publishers = {}

    data = read_data(sys.argv[1])
    # print data[:, 0]
    # print data[:, 0][0]
    # for i in range(100):
    #     if abs(data[:, 0][i] - data[:, 0][0]) < 0.01:
    #         print i

    # stop
    # plt.plot(data[:, 0][: 84], 'b')
    # plt.plot(y_track, 'g')
    # plt.show()
    # stop
    # dmp = DMP.DMPs_rhythmic(dmps=1, bfs=1000, dt=1.0 / 50.0)

    # dmps = {}
    # for c in range(data.shape[1]):
    #     dmps[robot_topics[c]] = DMP.DMPs_rhythmic(
    #         dmps=1, bfs=1000, dt=1.0 / 50.0)

    #     dmps[robot_topics[c]].imitate_path(y_des=data[:, c][: 300])

    # publishers[robot_topics[c]] = rospy.Publisher(robot_topics[c], Float64)

    # default timegain was 2.4
    data = data[: 188].transpose()  # 9 periods
    # print data.shape
    dmps = {}
    dmps = DMP.DMPs_rhythmic(
        dmps=12, bfs=1000, dt=1.0 / 50.0 * 2.0 * np.pi * 2.4 / 9)
    # print dmps.timesteps
    dmps.imitate_path(y_des=data)

    print dmps.w

    # res = dmps.rollout()
    # print res[0].shape
    # plt.plot(data[0], 'b')
    # plt.plot(res[0].transpose()[0], 'g')
    # plt.ylim([- 0.5, 0])
    # plt.show()
    # stop

    for c in range(data.shape[0]):

        publishers[robot_topics[c]] = rospy.Publisher(robot_topics[c], Float64)

    r = rospy.Rate(50)
    while not rospy.is_shutdown():
        i = 0
        step = dmps.step()[0]
        # for k, p in publishers.iteritems():
        for i in range(len(robot_topics)):

            # print dmps.step()[0]
            publishers[robot_topics[i]].publish(step[i])
            i += 1
        r.sleep()

    # y_track, dy_track, ddy_track = dmp.rollout()

    # plt.plot(data[:, 0][: 300], 'b')
    # plt.plot(y_track, 'g')
    # plt.show()
