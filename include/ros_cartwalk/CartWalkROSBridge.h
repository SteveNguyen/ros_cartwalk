//*****************************************************************************
//
// File Name	: 'CartWalkROSBridge.h'
// Author	: Steve NGUYEN
// Contact      : steve.nguyen@labri.fr
// Created	: mardi, février 24 2015
// Revised	:
// Version	:
// Target MCU	:
//
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//
//
// Notes:	notes
//
//*****************************************************************************

#if !defined(CARTWALKROSBRIDGE_H)
#define CARTWALKROSBRIDGE_H

#include <ros/ros.h>
#include <dynamic_reconfigure/server.h>
#include <iostream>

#include "Types/VectorLabel.hpp"
#include "std_msgs/Float64.h" //Float motor cmd
#include "std_srvs/Empty.h"
#include "ros_cartwalk/SetPhase.h"

#include "ros_cartwalk/SplineInfo.h"
#include "ros_cartwalk/cartwalkConfig.h"
#include "CartWalk/CartWalkProxy.hpp"


#include <math.h>
#include "math_basics.h"
/* #define DEG2RAD(deg) (deg*M_PI/180.0) */


class CartWalkROSBridge
{

  private:
        //Private member just to make Quentin happy :)
    ros::NodeHandle _nh;
    Leph::VectorLabel &_params;
    std::string _ns;
    Leph::CartWalkProxy &_walk;
  public:

        //Publishers for the legs
    ros::Publisher lleg_h_p;
    ros::Publisher lleg_h_r;
    ros::Publisher lleg_h_y;
    ros::Publisher lleg_k_p;
    ros::Publisher lleg_a_p;
    ros::Publisher lleg_a_r;

    ros::Publisher rleg_h_p;
    ros::Publisher rleg_h_r;
    ros::Publisher rleg_h_y;
    ros::Publisher rleg_k_p;
    ros::Publisher rleg_a_p;
    ros::Publisher rleg_a_r;

    ros::Publisher spline_info_pub;

    ros::ServiceServer setPhaseService;


    dynamic_reconfigure::Server<ros_cartwalk::cartwalkConfig> config_srv;
    dynamic_reconfigure::Server<ros_cartwalk::cartwalkConfig>::CallbackType params_cb;


    CartWalkROSBridge(ros::NodeHandle &nh, Leph::VectorLabel &params, Leph::CartWalkProxy &walk);
    void SendCommand(Leph::VectorLabel outputs);
    void configCallback(ros_cartwalk::cartwalkConfig &config, uint32_t level);
    bool setPhase(ros_cartwalk::SetPhase::Request& request, ros_cartwalk::SetPhase::Response& response);
    Leph::VectorLabel getParams();

};


#endif
