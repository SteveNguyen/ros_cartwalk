//*****************************************************************************
//
// File Name	: 'CartWalkROSBridge.cpp'
// Author	: Steve NGUYEN
// Contact      : steve.nguyen@labri.fr
// Created	: mardi, février 24 2015
// Revised	:
// Version	:
// Target MCU	:
//
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//
//
// Notes:	notes
//
//*****************************************************************************


#include "CartWalkROSBridge.h"

Leph::VectorLabel CartWalkROSBridge::getParams()
{
    return _params;
}

void CartWalkROSBridge::configCallback(ros_cartwalk::cartwalkConfig &config, uint32_t level)
{
    ROS_INFO("CONFIG CHANGED");

    _params("static:timeGain")=config.static_timeGain;
    _params("static:riseGain")=config.static_riseGain;
    _params("static:swingGain")=config.static_swingGain;
    _params("static:swingPhase")=config.static_swingPhase;
    _params("static:swingHeight")=config.static_swingHeight;
    _params("static:xOffset")=config.static_xOffset;
    _params("static:yOffset")=config.static_yOffset;
    _params("static:zOffset")=config.static_zOffset;
    _params("static:hipOffset")=config.static_hipOffset;
    _params("static:yLat")=config.static_yLat;
    _params("static:swingForce")=config.static_swingForce;
    _params("static:riseRatio")=config.static_riseRatio;
    _params("static:riseStepPhase")=config.static_riseStepPhase;
    _params("dynamic:enabled")=config.dynamic_enabled;
    _params("dynamic:step")=config.dynamic_step;
    _params("dynamic:lateral")=config.dynamic_lateral;
    _params("dynamic:turn")=config.dynamic_turn;

    std::cout<<_params<<std::endl;

}


bool CartWalkROSBridge::setPhase(ros_cartwalk::SetPhase::Request& request, ros_cartwalk::SetPhase::Response& response)
{
    _walk.setPhase(request.phase);
    ROS_INFO("SETTING Phase to %f",request.phase);
    return true;
}


CartWalkROSBridge::CartWalkROSBridge(ros::NodeHandle &nh, Leph::VectorLabel &params, Leph::CartWalkProxy &walk): _nh(nh), _params(params), _walk(walk)
{
    // _nh = nh;
    // _params=params;
    _ns=ros::this_node::getNamespace();
        //init publishers
    lleg_h_p=_nh.advertise<std_msgs::Float64> (_ns+"/lleg_h_p_controller/command",0);
    lleg_h_r=_nh.advertise<std_msgs::Float64> (_ns+"/lleg_h_r_controller/command",0);
    lleg_h_y=_nh.advertise<std_msgs::Float64> (_ns+"/lleg_h_y_controller/command",0);
    lleg_k_p=_nh.advertise<std_msgs::Float64> (_ns+"/lleg_k_p_controller/command",0);
    lleg_a_p=_nh.advertise<std_msgs::Float64> (_ns+"/lleg_a_p_controller/command",0);
    lleg_a_r=_nh.advertise<std_msgs::Float64> (_ns+"/lleg_a_r_controller/command",0);

    rleg_h_p=_nh.advertise<std_msgs::Float64> (_ns+"/rleg_h_p_controller/command",0);
    rleg_h_r=_nh.advertise<std_msgs::Float64> (_ns+"/rleg_h_r_controller/command",0);
    rleg_h_y=_nh.advertise<std_msgs::Float64> (_ns+"/rleg_h_y_controller/command",0);
    rleg_k_p=_nh.advertise<std_msgs::Float64> (_ns+"/rleg_k_p_controller/command",0);
    rleg_a_p=_nh.advertise<std_msgs::Float64> (_ns+"/rleg_a_p_controller/command",0);
    rleg_a_r=_nh.advertise<std_msgs::Float64> (_ns+"/rleg_a_r_controller/command",0);

    spline_info_pub=_nh.advertise<ros_cartwalk::SplineInfo> ("/ros_cartwalk/splines",0);

        //service for setPhase()

    setPhaseService = _nh.advertiseService(_ns+"/set_phase", &CartWalkROSBridge::setPhase,this);


        //f***ing esoteric boost stuff
    params_cb=boost::bind(&CartWalkROSBridge::configCallback,this, _1, _2);
    config_srv.setCallback(params_cb);
}


void CartWalkROSBridge::SendCommand(Leph::VectorLabel outputs){

    std_msgs::Float64 tmp;
    tmp.data=DEG2RAD(outputs("output:left hip pitch"));
    lleg_h_p.publish(tmp);
    tmp.data=DEG2RAD(outputs("output:left hip roll"));
    lleg_h_r.publish(tmp);
    tmp.data=DEG2RAD(outputs("output:left hip yaw"));
    lleg_h_y.publish(tmp);
    tmp.data=DEG2RAD(outputs("output:left knee"));
    lleg_k_p.publish(tmp);
    tmp.data=DEG2RAD(outputs("output:left foot pitch"));
    lleg_a_p.publish(tmp);
    tmp.data=DEG2RAD(outputs("output:left foot roll"));
    lleg_a_r.publish(tmp);

    tmp.data=DEG2RAD(outputs("output:right hip pitch"));
    rleg_h_p.publish(tmp);
    tmp.data=DEG2RAD(outputs("output:right hip roll"));
    rleg_h_r.publish(tmp);
    tmp.data=DEG2RAD(outputs("output:right hip yaw"));
    rleg_h_y.publish(tmp);
    tmp.data=DEG2RAD(outputs("output:right knee"));
    rleg_k_p.publish(tmp);
    tmp.data=DEG2RAD(outputs("output:right foot pitch"));
    rleg_a_p.publish(tmp);
    tmp.data=DEG2RAD(outputs("output:right foot roll"));
    rleg_a_r.publish(tmp);


    ros_cartwalk::SplineInfo sp;

    sp.phase=outputs("info:phase");
    sp.left_spline_x=outputs("info:left spline X");
    sp.left_spline_y=outputs("info:left spline Y");
    sp.left_spline_z=outputs("info:left spline Z");

    sp.right_spline_x=outputs("info:right spline X");
    sp.right_spline_y=outputs("info:right spline Y");
    sp.right_spline_z=outputs("info:right spline Z");

    spline_info_pub.publish(sp);

}
