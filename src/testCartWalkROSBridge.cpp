//*****************************************************************************
//
// File Name	: 'testCartWalkROSBridge.cpp'
// Author	: Steve NGUYEN
// Contact      : steve.nguyen@labri.fr
// Created	: mardi, février 24 2015
// Revised	:
// Version	:
// Target MCU	:
//
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//
//
// Notes:	notes
//
//*****************************************************************************


#include "CartWalkROSBridge.h"
#include <ros/ros.h>

#include <iostream>
#include "Types/VectorLabel.hpp"
#include "CartWalk/CartWalkProxy.hpp"


int main(int argc, char* argv[])
{
    Leph::CartWalkProxy walk;
    Leph::VectorLabel outputs = walk.buildOutputs();
    Leph::VectorLabel params = walk.buildParams();

    params("dynamic:enabled") = 0;
    params("dynamic:step") = 0;

    std::cout << outputs << std::endl;
    std::cout << params << std::endl;



        //ROS bridge
    ros::init(argc, argv, "test_ros_cartwalk_bridge");
    ros::NodeHandle nh;
    CartWalkROSBridge bridge(nh,params,walk);
    string ns=ros::this_node::getNamespace();

    std::cout<<"Namespace: "<<ns<<std::endl;
    ros::Rate r(50); // 50 hz
    ros::Time t = ros::Time::now();

    double prev = t.toSec();

    double dt = t.toSec()-prev;

    while (ros::ok())
    {
        t = ros::Time::now();
        dt = t.toSec()-prev;
        walk.exec(dt, params);
        // walk.exec(dt, bridge.getParams());

            //std::cout << t.toSec()<<" "<<prev<<" "<<dt<<" "<<walk.lastOutputs();
        bridge.SendCommand(walk.lastOutputs());
        ros::spinOnce();//Update the loop
        r.sleep();
        prev=t.toSec();
    }


    return 0;

}
